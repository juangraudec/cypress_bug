describe("Section navigation", function() {
  beforeEach(() => {
    cy.server();
    cy.route("GET", "*", "fixture:index");
    cy.visit("/");
  });

  it("should scrooll", function() {
    // cy.get('[href="#article3"]').should("not.be.visible");
    cy.url().should("not.contains", "#article3");
    cy.get('[href="#article3"]').click();
    cy.url().should("contains", "#article3");
    cy.wait(100);
    cy.get('[href="#top"]')
      .last()
      .click();
    cy.url().should("not.contains", "#article3");
    cy.url().should("contains", "#top");
    cy.wait(100);
    cy.get('[href="#article2"]').click();
    cy.window().then(win => {
      expect(win.pageYOffset).to.gt(2000);
    });
    cy.url().should("contains", "#article2");
    cy.wait(100);
    cy.get('[href="#top"]')
      .last()
      .click();
    cy.url().should("contains", "#top");
    cy.window().then(win => {
      expect(win.pageYOffset).to.eq(0);
    });
  });
});
